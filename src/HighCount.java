import java.util.HashMap;
import java.util.Map;

public class HighCount {
    private static HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();

    public static void main(String[] args) {

        int[] arr = {2, 2, 3, 3, 4, 4, 5, 5, 5, 5, 5};
        int n = arr.length;
        insert(arr, n);
        int answer = maximum();
        System.out.println("RESULT is " + answer);
    }

    static void insert(int[] arr, int n) {

        for (int i = 0; i < n; i++) {
            int key = arr[i];
            if (map.containsKey(key)) {
                int freqCount = map.get(key);
                freqCount++;
                map.put(key, freqCount);

            } else {
                map.put(key, 1);
            }
        }
    }

    //Find maximum
    static int maximum() {
        int max = 0, result = -1;
        for (Map.Entry<Integer, Integer> m : map.entrySet()) {
            if (max < m.getValue()) {
                max = m.getValue();
                result = m.getKey();
            }

        }
        return result;
    }
}

